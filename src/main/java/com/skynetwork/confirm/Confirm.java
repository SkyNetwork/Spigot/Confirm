package com.skynetwork.confirm;

import com.skynetwork.sdk.Database.Accounts.AccountLevelController;
import com.skynetwork.sdk.SDK;
import com.skynetwork.sdk.Utilities.Messages.MessageDefaults;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import com.skynetwork.sdk.Debugger.Logit;

import java.util.Objects;

public class Confirm extends JavaPlugin implements CommandExecutor {
    private Logit Logger;

    private void sendMessage(Player player, MessageDefaults.MessageType type, String message) {
        player.sendMessage(
            MessageDefaults.createPrefix(this.getName()) +
            MessageDefaults.getSchemeColor(type) + message
        );
    }

    @Override
    public void onEnable() {
        Logger = new Logit(this);
        Logger.Log("Starting Plugin..", Logit.Level.Info);

        getCommand("confirm").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;

        if (args.length == 0)
            return false;

        Player Sender = (Player)sender;

        Logger.Log(String.format("Attempting to verify %s with \"%s\"",
            Sender.getName(), args[0]), Logit.Level.Info
        );

        AccountLevelController accountLevelController =
            ((AccountLevelController)SDK.getController(AccountLevelController.class));

        if (accountLevelController.isAccount(Sender)) {
            Logger.Log(String.format("Account with name %s exists",
                Sender.getName()), Logit.Level.Info
            );

            if (!Objects.equals(
                accountLevelController.getAccountLevel(Sender),
                AccountLevelController.AccountLevel.Unconfirmed)) {

                Logger.Log(String.format("Account with name %s already verified",
                    Sender.getName()), Logit.Level.Info
                );

                sendMessage(Sender, MessageDefaults.MessageType.Warning,
                    "Your account has already been confirmed."
                );

                return true;
            }
        }

        if (!accountLevelController.isAccount(args[0])) {
            Logger.Log(String.format("Verification code \"%s\" is invalid",
                args[0]), Logit.Level.Info
            );

            sendMessage(Sender, MessageDefaults.MessageType.Error,
                "Invalid verification code."
            );

            return true;
        }

        Logger.Log(String.format("Verifying %s with ID %s",
            Sender.getName(), args[0]), Logit.Level.Info
        );

        accountLevelController.updateAccountLevel(
            args[0], Sender,
            AccountLevelController.AccountLevel.Player
        );

        Logger.Log(String.format("Verified %s with ID %s",
            Sender.getName(), args[0]), Logit.Level.Info
        );

        sendMessage(Sender, MessageDefaults.MessageType.Info,
            "Your account has been verified."
        );

        return true;
    }
}
